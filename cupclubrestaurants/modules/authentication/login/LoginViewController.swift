//
//  LoginViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 10/05/2021.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var fieldEmailOrPhone: SkyFloatingLabelTextField!
    
    @IBOutlet weak var fieldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnPasswordVisibility: UIButton!
    private  var isPasswordVisible : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        #if DEBUG
        setUpTestingData()
        #endif
    }
    
    private func setUpTestingData() {
        fieldEmailOrPhone.text = "user2@example.com"
        fieldPassword.text = "2 user 2"
    }
    
    @IBAction func openTermsAndConditionsPressed(_ sender: Any) {
        
    }
    
    @IBAction func openPrivacyPolicyPressed(_ sender: Any) {
        
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        authorizeUser()
    }
    
    @IBAction func goBackPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        self.pushVC(name: "ResetPasswordViewController", sb: Storyboards.authentication)
    }
    
    
    @IBAction func passwordVisibilityPressed(_ sender: Any) {
        if (self.isPasswordVisible) {
            self.btnPasswordVisibility.setImage(UIImage(named: "ic_pass_hidden"), for: .normal)
            self.fieldPassword.isSecureTextEntry = true
            self.isPasswordVisible = false
        }else {
            self.btnPasswordVisibility.setImage(UIImage(named: "ic_pass_visible"), for: .normal)
            self.fieldPassword.isSecureTextEntry = false
            self.isPasswordVisible = true
        }
    }
    
    
    private func authorizeUser() {
        validate()
    }
    
    private func validate() {
        let username = fieldEmailOrPhone.getTextFromField()
        let password = fieldPassword.getTextFromField()
        
        //username
        if (!username.isValidEmail()) {
            fieldEmailOrPhone.errorMessage = "enter_valid_email".localized
            return
        }
        fieldEmailOrPhone.errorMessage = ""
        
        if (password.count == 0) {
            fieldPassword.errorMessage = "enter_valid_password".localized
            return
        }
        fieldPassword.errorMessage = ""
        
//        if (self.isValidPassword(password: password) == false) {
//            fieldPassword.errorMessage = "enter_valid_password_regex".localized
//            return
//        }
       // fieldPassword.errorMessage = ""
        
        //authenticate api call
        self.showLoading()
        getNetworkManager().authenticate(username: username, password: password, completion: { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.hideLoading()
            switch result {
            case .success(let response):
                strongSelf.updateCredentials(accessToken: response.accessToken, refreshToken: response.refreshToken, userId: response.userID)
                strongSelf.showBanner(title: "success", message: "\(response.expiresIn ?? 0)", type: .success)
                self?.validateNextScreenToOpen()
            case .failure(let error):
                strongSelf.showBanner(title: "failure", message: error.localizedDescription, type: .error)
            }
        })
        
    }
    
    private func validateNextScreenToOpen() {
        isLocationPermissionGranted { (isGranted) in
            if (isGranted) {
                self.goHome()
            }else {
                self.presentVC(name: "EnableLocationViewController", sb: Storyboards.authentication)
            }
        }
    }
    
    private func getUserProfile() {
        self.showLoading()
        getNetworkManager().getProfile(completion: { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.hideLoading()
            switch result {
            case .success(let response):
                strongSelf.showBanner(title: "success", message: "\(response)", type: .success)
            case .failure(let error):
                strongSelf.showBanner(title: "failure", message: error.localizedDescription, type: .error)
            }
        })
    }
    
}
