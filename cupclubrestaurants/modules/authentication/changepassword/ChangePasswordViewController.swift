//
//  ChangePasswordViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: BaseViewController {
    
    
    //new password
    @IBOutlet weak var fieldNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNewPasswordVisiblity: UIButton!
    private  var isNewPasswordVisible : Bool = false
    
    //confirm new password
    @IBOutlet weak var fieldConfirmNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnConfirmNewPasswordVisibility: UIButton!
    private  var isConfirmNewPasswordVisible : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newPasswordVisibilityPressed(_ sender: Any) {
        if (self.isNewPasswordVisible) {
            self.btnNewPasswordVisiblity.setImage(UIImage(named: "ic_pass_hidden_white"), for: .normal)
            self.fieldNewPassword.isSecureTextEntry = true
            self.isNewPasswordVisible = false
        }else {
            self.btnNewPasswordVisiblity.setImage(UIImage(named: "ic_pass_visible_white"), for: .normal)
            self.fieldNewPassword.isSecureTextEntry = false
            self.isNewPasswordVisible = true
        }
    }
    
    @IBAction func confirmNewPasswordVisiblityPressed(_ sender: Any) {
        if (self.isConfirmNewPasswordVisible) {
            self.btnConfirmNewPasswordVisibility.setImage(UIImage(named: "ic_pass_hidden_white"), for: .normal)
            self.fieldConfirmNewPassword.isSecureTextEntry = true
            self.isConfirmNewPasswordVisible = false
        }else {
            self.btnConfirmNewPasswordVisibility.setImage(UIImage(named: "ic_pass_visible_white"), for: .normal)
            self.fieldConfirmNewPassword.isSecureTextEntry = false
            self.isConfirmNewPasswordVisible = true
        }
    }
    
    @IBAction func changePasswordPressed(_ sender: Any) {
        
    }
    
    
    
}
