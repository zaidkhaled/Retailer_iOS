//
//  ThirdOnBoardingCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 09/05/2021.
//

import UIKit

class ThirdOnBoardingCell: BaseCVCell {
    
    @IBOutlet weak var lblTitle: NormalLabel!
    
    @IBOutlet weak var ivPackaging: UIImageView!
    
}
