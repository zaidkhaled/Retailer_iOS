//
//  SecondOnBoardingCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 09/05/2021.
//

import UIKit

class SecondOnBoardingCell: BaseCVCell {
    
    @IBOutlet weak var lblTitle: NormalLabel!
    
    @IBOutlet weak var ivQrCode: UIImageView!
    
}
