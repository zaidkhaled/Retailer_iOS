//
//  FirstOnBoardingCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 09/05/2021.
//

import UIKit

class FirstOnBoardingCell: BaseCVCell {
   
    @IBOutlet weak var ivLogo: UIImageView!
    
    @IBOutlet weak var lblContent: NormalLabel!
    
}
