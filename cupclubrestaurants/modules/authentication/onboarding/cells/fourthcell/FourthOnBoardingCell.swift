//
//  FourthOnBoardingCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 10/05/2021.
//

import UIKit
import SkyFloatingLabelTextField

enum RegisterFormField {
    case name
    case email
    case phoneNumber
    case retailerName
}

protocol FourthOnBoardingCellProtocol {
    func onTermsAndConditionsPressed()
    func onPrivacyPolicyPressed()
    func onBackPressed()
    func onSubmitPressed(name : String, email : String, phoneNumber : String, retailerName : String)
    func onLoginPressed()
}

class FourthOnBoardingCell: BaseCVCell {
    
    @IBOutlet weak var fieldName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var fieldEmail: SkyFloatingLabelTextField!
    
    @IBOutlet weak var fieldPhoneNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var fieldRetailerName: SkyFloatingLabelTextField!
    
    var delegate : FourthOnBoardingCellProtocol?
    
    @IBAction func openTermsAndConditionsPressed(_ sender: Any) {
        delegate?.onTermsAndConditionsPressed()
    }
    
    @IBAction func openPrivacyPolicyPressed(_ sender: Any) {
        delegate?.onPrivacyPolicyPressed()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        delegate?.onBackPressed()
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        validate()
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        delegate?.onLoginPressed()
    }
    
    private func validate() {
        let name = fieldName.getTextFromField()
        let email = fieldEmail.getTextFromField()
        let phoneNumber = fieldPhoneNumber.getTextFromField()
        let retailerName = fieldRetailerName.getTextFromField()
        
        //name
        if (!name.isValidPersonName()) {
            fieldName.errorMessage = "enter_valid_name".localized
            return
        }
        fieldName.errorMessage = ""
        
        //email
        if (!email.isValidEmail()) {
            fieldEmail.errorMessage = "enter_valid_email".localized
            return
        }
        fieldEmail.errorMessage = ""
        
        //phone number
        if (!phoneNumber.isValidPhoneNumber()) {
            fieldPhoneNumber.errorMessage = "enter_valid_phone".localized
            return
        }
        fieldPhoneNumber.errorMessage = ""
        
        //retailer name
        if (!retailerName.isValidPersonName()) {
            fieldRetailerName.errorMessage = "enter_valid_retailername".localized
            return
        }
        fieldRetailerName.errorMessage = ""
        
        delegate?.onSubmitPressed(name: name, email: email, phoneNumber: phoneNumber, retailerName: retailerName)
    }
    
}
