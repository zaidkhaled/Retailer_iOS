//
//  OnBoardingViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 09/05/2021.
//

import UIKit
import CHIPageControl
import Player

enum OnBoardingContent {
    case photo
    case video
    case login
}

class OnBoardingViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: CHIPageControlAji!
    
    @IBOutlet weak var ivOnBoarding: UIImageView!
    
    @IBOutlet weak var viewOverLay: UIView!
    
    private var currentPage : Int = 0
    
    private var onBoardingCells : [UICollectionViewCell] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    //video
    @IBOutlet weak var videoView: UIView!
    var player : Player?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpCells()
        
        setUpCollection()
        
        populateData()
        
        setupVideoPlayer()
    }
    
    private func displayOnBoardingContent(type : OnBoardingContent) {
        switch type {
        case .photo:
            ivOnBoarding.isHidden = false
            videoView.isHidden = true
            UIView.animate(withDuration: 0.3) {
                self.viewOverLay.backgroundColor = UIColor.black
                self.viewOverLay.alpha = 0.68
            }
            break
        case .video:
            ivOnBoarding.isHidden = true
            videoView.isHidden = false
            player?.playFromCurrentTime()
            UIView.animate(withDuration: 0.3) {
                self.viewOverLay.backgroundColor = UIColor.black
                self.viewOverLay.alpha = 0.45
            }
            break
        case .login:
            UIView.animate(withDuration: 0.3) {
                self.viewOverLay.backgroundColor = UIColor.white
                self.viewOverLay.alpha = 1
            }
        }
    }
    
    private func setUpCells() {
        collectionView.register(UINib(nibName: "FirstOnBoardingCell",bundle: nil), forCellWithReuseIdentifier: "FirstOnBoardingCell")
        collectionView.register(UINib(nibName: "SecondOnBoardingCell",bundle: nil), forCellWithReuseIdentifier: "SecondOnBoardingCell")
        collectionView.register(UINib(nibName: "ThirdOnBoardingCell",bundle: nil), forCellWithReuseIdentifier: "ThirdOnBoardingCell")
        collectionView.register(UINib(nibName: "FourthOnBoardingCell",bundle: nil), forCellWithReuseIdentifier: "FourthOnBoardingCell")
    }
    
    private func setUpCollection() {
        collectionView.delegate = self
        collectionView.dataSource = self
        pageControl.hidesForSinglePage = true
    }
    
    private func populateData() {
        onBoardingCells = [
            collectionView.dequeueReusableCell(withReuseIdentifier:"FirstOnBoardingCell", for: IndexPath(row: 0, section: 0)),
            collectionView.dequeueReusableCell(withReuseIdentifier:"SecondOnBoardingCell", for: IndexPath(row: 1, section: 0)),
            collectionView.dequeueReusableCell(withReuseIdentifier:"ThirdOnBoardingCell", for: IndexPath(row: 2, section: 0)),
            collectionView.dequeueReusableCell(withReuseIdentifier:"FourthOnBoardingCell", for: IndexPath(row: 3, section: 0))
        ]
    }
    
    private func setupVideoPlayer() {
        if (self.player == nil) {
            self.player = Player()
            self.player?.playbackDelegate = self
            self.player?.playerDelegate = self
            self.player?.playbackLoops = true
            self.player?.volume = 0
            self.player?.fillMode = .resizeAspectFill
            self.player?.view.frame = self.videoView.bounds
            
            self.addChild(self.player!)
            self.videoView.addSubview(self.player!.view)
            self.videoView.bindFrameToSuperviewBounds()
            self.videoView.layoutSubviews()
            self.player!.didMove(toParent: self)
        }
        self.playVideo()
    }
    
    private func playVideo() {
        let url = Bundle.main.url(forResource: "onboarding_video", withExtension: "mp4")
        self.player?.url = url
        self.player?.playFromBeginning()
    }
    
    private func validateCurrentPageUI() {
        switch currentPage {
        case 1, 2:
            displayOnBoardingContent(type: .video)
            break
        case 3:
            displayOnBoardingContent(type: .login)
            break
        default:
            displayOnBoardingContent(type: .photo)
            break
        }
        
        self.pageControl.set(progress: currentPage, animated: true)
        self.pageControl.isHidden = currentPage == 3 ? true : false
    }
}


extension OnBoardingViewController : UICollectionViewDelegate , UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onBoardingCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let onBoardingCell = onBoardingCells[indexPath.row]
        if let requestAccountCell = onBoardingCell as? FourthOnBoardingCell {
            requestAccountCell.delegate = self
        }
        return onBoardingCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height:self.collectionView.bounds.height)
    }
}

extension OnBoardingViewController : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        validateCurrentPageUI()
    }
}


extension OnBoardingViewController: PlayerPlaybackDelegate {
    func playerCurrentTimeDidChange(_ player: Player) {
        //nth
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
        //nth
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
        //nth
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
        //nth
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
        //nth
    }
}

extension OnBoardingViewController: PlayerDelegate {
    
    func playerReady(_ player: Player) {
        print("\(#function) ready")
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        print("\(#function) \(player.playbackState.description)")
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        print("\(#function) error.description")
    }
    
}

extension OnBoardingViewController : FourthOnBoardingCellProtocol {
    func onTermsAndConditionsPressed() {
        //todo
    }
    
    func onPrivacyPolicyPressed() {
        //todo
    }
    
    func onBackPressed() {
        currentPage -= 1
        let indexPath = IndexPath(row: currentPage, section: 0)
        collectionView.isPagingEnabled = false
        collectionView.scrollToItem(
            at: indexPath,
            at: .centeredHorizontally,
            animated: true
        )
        collectionView.isPagingEnabled = true
        validateCurrentPageUI()
    }
    
    func onSubmitPressed(name: String, email: String, phoneNumber: String, retailerName: String) {
        //todo
    }
    
    func onLoginPressed() {
        self.pushVC(name: "LoginViewController", sb: Storyboards.authentication)
    }
    
    
}
