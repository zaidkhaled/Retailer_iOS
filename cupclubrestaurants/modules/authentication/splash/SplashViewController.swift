//
//  SplashViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import UIKit

class SplashViewController: BaseViewController {

    
    @IBOutlet weak var ivSplashLogo: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startLogoAnimation()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.goToNextScreen()
        }
    }
    
    private func goToNextScreen() {
        presentVC(name: "OnBoardingNavigationController", sb: Storyboards.authentication)
    }
    
    private func startLogoAnimation() {
        self.ivSplashLogo.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.5, animations: {() -> Void in
            self.ivSplashLogo.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }

}
