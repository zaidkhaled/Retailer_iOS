//
//  EnableLocationViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit
import CoreLocation
import SwiftLocation

class EnableLocationViewController: BaseViewController, CLLocationManagerDelegate {

    @IBOutlet weak var ivenableLocation: UIImageView!
    
    @IBOutlet weak var lblEnableLocation: NormalLabel!
    
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startViewsAnimation()
    }
    
    private func startViewsAnimation() {
        self.ivenableLocation.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        self.lblEnableLocation.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.5, animations: {() -> Void in
            self.ivenableLocation.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.lblEnableLocation.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    private func requestLocationCoordinates() {
        self.showLoading()
        SwiftLocation.gpsLocation().then { result in
            self.hideLoading()
            switch result {
            case .success(let newData):
                print("SUCCESS::")
                self.updateUserLocation(newData.coordinate.latitude, newData.coordinate.longitude)
                self.proceedToNextView()
            case .failure(let error):
                print("FAIL::")
                print(error.localizedDescription)
                self.checkIfLocationIsEnabled()
            }
        }
    }
    
    @IBAction func enableLocationPressed(_ sender: Any) {
        requestLocationCoordinates()
    }
    
    private func checkIfLocationIsEnabled() {
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        case .denied, .restricted:
            let alert = UIAlertController(title: "location_is_disabled".localized, message: "please_enable_location".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok".localized, style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            return
        case .authorizedAlways, .authorizedWhenInUse:
            self.proceedToNextView()
            break
        @unknown default:
            self.proceedToNextView()
            break
        }
    }
    
    private func proceedToNextView() {
        goHome()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedAlways || status == .authorizedWhenInUse) {
            self.proceedToNextView()
        }
    }
    
    @IBAction func laterPressed(_ sender: Any) {
        self.proceedToNextView()
    }

}
