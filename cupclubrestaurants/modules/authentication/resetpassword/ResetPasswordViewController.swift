//
//  ResetPasswordViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit
import SkyFloatingLabelTextField

class ResetPasswordViewController: BaseViewController {
    
    
    @IBOutlet weak var fieldEmail: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resetPressed(_ sender: Any) {
        pushVC(name: "ChangePasswordViewController", sb: Storyboards.authentication)
    }
    
}
