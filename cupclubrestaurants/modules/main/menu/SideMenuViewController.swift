//
//  SideMenuViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class SideMenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var menuTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        
    }
    
    private func setUpTableView() {
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 20, right: 0)
        menuTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SideMenuCell = menuTableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    @IBAction func ordersPressed(_ sender: Any) {
        pushVC(name: "OrdersHistoryViewController", sb: Storyboards.order)
    }
    
    @IBAction func howItWorksPressed(_ sender: Any) {
        pushVC(name: "HowItWorksViewController", sb: Storyboards.more)
    }
    
    @IBAction func helpPressed(_ sender: Any) {
        pushVC(name: "HelpViewController", sb: Storyboards.more)
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
        
    }
    
    
}
