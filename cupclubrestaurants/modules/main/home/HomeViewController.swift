//
//  HomeViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var ivRetailerLogo: CircleImage!
    
    @IBOutlet weak var lblRetailerJoinDate: NormalLabel!
    
    @IBOutlet weak var lblRetailerAddress: NormalLabel!
    
    @IBOutlet weak var ivRetailerBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func startScanningPressed(_ sender: Any) {
        pushVC(name: "ScanCustomerViewController", sb: Storyboards.order)
    }
}
