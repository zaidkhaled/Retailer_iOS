//
//  SideMenuCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class SideMenuCell: BaseTVCell {
    //100

    @IBOutlet weak var lblTitle: NormalLabel!
    
    @IBOutlet weak var lblContent: NormalLabel!
    
}
