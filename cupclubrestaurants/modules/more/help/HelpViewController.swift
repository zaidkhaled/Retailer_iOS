//
//  HelpViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 19/05/2021.
//

import UIKit
import MultilineTextField

class HelpViewController: BaseViewController {

    @IBOutlet weak var fieldMessage: MultilineTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    private func setUpMultiLineMessageField() {
        fieldMessage.placeholder = "type_here".localized
        fieldMessage.placeholderColor = UIColor.multiLinePlaceHolderColor
        fieldMessage.font = UIFont(name: getFontName(type: Enums.FONT_TYPE.normal_semibold), size: 18.0)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        
    }
    
    @IBAction func faqsPressed(_ sender: Any) {
        
    }
    
}
