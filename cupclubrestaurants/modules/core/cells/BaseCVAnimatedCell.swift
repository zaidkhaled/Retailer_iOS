//
//  BaseCVAnimatedCell.swift
//  eaigapp
//
//  Created by Zaid Khaled on 15/03/2021.
//

import UIKit

class BaseCVAnimatedCell: UICollectionViewCell {
    
    
    override var isHighlighted: Bool{
        didSet{
            if isHighlighted {
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                    self.transform = self.transform.scaledBy(x: 0.90, y: 0.90)
                }, completion: nil)
            }else{
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                }, completion: nil)
            }
        }
    }
}
