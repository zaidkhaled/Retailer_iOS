//
//  BaseTVCell.swift
//  FlyUsVip
//
//  Created by Zaid Khaled on 10/20/20.
//

import UIKit
import DatePickerDialog
import SwiftDate
import CoreLocation

class BaseTVCell: UITableViewCell {
    
    func getDatePicker(withCancel : Bool) -> DatePickerDialog {
        var picker = DatePickerDialog()
        if (withCancel) {
            picker = DatePickerDialog(showCancelButton: true)
        }
        if #available(iOS 13.4, *) {
            picker.datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        return picker
    }
    
    func getStringDateWithFormat(dateStr : String, outputFormat : String) -> String {
        let date = DateInRegion(dateStr, format: Enums.DATE_FORMATS.api_date, region: .current)
        let dateFinal = date?.toFormat(outputFormat)
        return dateFinal ?? "---"
    }
    
    func getStringFromDate(date : Date, outputFormat : String) -> String {
        let dateFinal = date.toFormat(outputFormat)
        return dateFinal
    }
    
    func getDateFromString(dateStr : String) -> Date {
        return dateStr.toDate()?.date ?? Date()
    }
    
    func getNowDate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: "UTC")
        
        let date = formatter.string(from: Date())
        let formattedDate = self.getDateFromString(dateStr: date)
        
        return formattedDate
    }
    
    func loadUser() -> String? {
        
        let keychain = KeychainSwift()
        let storedData = keychain.getData("UserInfo")
        if storedData != nil {
            let userInfo = try! JSONDecoder().decode(String?.self, from: storedData!)
            return userInfo
        }
        return nil
    }
    
    func isLoggedInNoAction() -> Bool {
        let userId = getUserId()
        if (userId.count > 0) {
            return true
        }else {
            return false
               }
    }
    
    
    //keychain functions
    func getUserId() -> String{
        return ""
    }
    
    func getAccessToken() -> String{
        let keychain = KeychainSwift()
        let storedData = keychain.getData("accessToken")
        if storedData != nil {
            let accessToken = try! JSONDecoder().decode(String.self, from: storedData!)
            return accessToken
        }
        return ""
    }
    
    func showTimePicker(completion:@escaping(_ time : Date)-> Void) {
        let minDate = Date().dateAt(.startOfDay)
        let maxDate = Date().dateAt(.endOfDay)
        
        let picker = DatePickerDialog()
        if #available(iOS 13.4, *) {
            picker.datePicker.preferredDatePickerStyle = .wheels
            picker.tintColor = UIColor.colorPrimary
        } else {
            // Fallback on earlier versions
        }
        picker.datePicker.timeZone = .current
        picker.show("select_time".localized, doneButtonTitle: "done".localized, cancelButtonTitle: "cancel".localized, defaultDate: minDate, minimumDate: minDate, maximumDate: maxDate, datePickerMode: .time) { (date) in
            
            completion(date ?? Date())
        }
    }
    
    func openUrl(str : String) {
        if (str.count > 0 && str.contains(find: "http")) {
            let url = URL(string: str)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    
}
