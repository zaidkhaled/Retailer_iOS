//
//  BaseViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import UIKit
import SVProgressHUD
import CoreLocation
import MapKit
import SwiftDate
import DatePickerDialog
import Hero
import FirebaseAnalytics
import GoogleMaps
import GooglePlaces
import Toast_Swift

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.swipeToPop()
        SVProgressHUD.setDefaultMaskType(.clear)
        // Do any additional setup after loading the view.
    }
    
    
    func swipeToPop() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func showLoading() {
        SVProgressHUD.setBackgroundColor(UIColor.colorPrimary)
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
    
    func presentNav(name : String, sb : UIStoryboard) {
        let initialViewControlleripad : UIViewController = sb.instantiateViewController(withIdentifier: name) as! UINavigationController
        initialViewControlleripad.modalPresentationStyle = .fullScreen
        self.present(initialViewControlleripad, animated: true, completion: {})
    }
    
    func presentTabBar(name : String) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UIViewController = sb.instantiateViewController(withIdentifier: name) as! UITabBarController
        initialViewControlleripad.modalPresentationStyle = .fullScreen
        self.present(initialViewControlleripad, animated: true, completion: {})
    }
    
    func showBanner(title:String, message:String,type: Enums.APP_MESSAGE) {
        // create a new style
        var style = ToastStyle()
        
        style.titleFont = UIFont(name: getFontName(type: Enums.FONT_TYPE.normal_medium), size: 15.0)!
        style.messageFont = UIFont(name: getFontName(type: Enums.FONT_TYPE.normal_regular), size: 13.0)!
        
        // this is just one of many style options
        switch type {
        case .info:
            style.messageColor = UIColor.INFO
            break
        case .success:
            style.messageColor = UIColor.SUCCESS
            break
        case .error:
            style.messageColor = UIColor.ERROR
            break
        default:
            style.messageColor = UIColor.INFO
        }
        
        self.view.makeToast(message, duration: 3.0, position: .bottom, title : title, style: style)
        
    }
    
    
    func presentVC(name : String, sb : UIStoryboard) {
        let vc = sb.instantiateViewController(withIdentifier: name)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func getNetworkManager() -> NetworkManager {
        return NetworkManager()
    }
    
    func presentVCSheetModal(name : String, sb : UIStoryboard) {
        let vc = sb.instantiateViewController(withIdentifier: name)
        self.present(vc, animated: true, completion: nil)
    }
    
    func goHome() {
        presentVC(name: "HomeNavigationController", sb: Storyboards.main)
    }
    
    func pushVC(name : String, sb : UIStoryboard) {
        let vc = sb.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlert(title:String,
                   message:String,
                   buttonText:String = "Ok".localized,
                   actionHandler:(()->())? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: buttonText, style: UIAlertAction.Style.default) { (action) in
            actionHandler?()
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String,
                   message: String,
                   actionTitle: String,
                   cancelTitle: String,
                   actionHandler:(()->Void)?,
                   cancelHandler:(()->Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            actionHandler?()
        }
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
            cancelHandler?()
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithCancel(title: String,
                             message: String,
                             actionTitle: String,
                             cancelTitle: String,
                             actionHandler:(()->Void)?,
                             cancelHandler:(()->Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: actionTitle, style: .default) { (action) in
            actionHandler?()
        }
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .default) { (action) in
            cancelHandler?()
        }
        let dismissAction = UIAlertAction(title: "cancel".localized, style: .cancel) {
            UIAlertAction in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getStringDateWithFormat(dateStr : String, outputFormat : String) -> String {
        let date = dateStr.toDate(Enums.DATE_FORMATS.api_date, region: iHospitalRegion())
        let dateFinal = date?.toFormat(outputFormat)
        return dateFinal ?? "---"
    }
    
    func getDateTwilio(dateStr : String, outputFormat : String) -> String {
        let date = dateStr.toDate(nil, region: iHospitalRegion())
        let dateFinal = date?.toFormat(outputFormat)
        return dateFinal ?? "---"
    }
    
    func getStringFromDate(date : Date, outputFormat : String) -> String {
        let region = iHospitalRegion()
        let dateFinal = date.in(region: region).toFormat(outputFormat)
        return dateFinal
    }
    
    func getDateFromString(dateStr : String) -> Date {
        return dateStr.toDate(Enums.DATE_FORMATS.api_date, region: self.iHospitalRegion())?.date ?? Date()
    }
    
    func getDateFromHour(hour : String) -> Date {
        return hour.toDate(Enums.DATE_FORMATS.hh_mm_a, region: self.iHospitalRegion())?.date ?? Date()
    }
    
    func iHospitalRegion() -> Region {
        return Region(calendar: Calendars.gregorian, zone: Zones.gmt, locale: Locales.english)
    }
    
    func getNowDate() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: "UTC")
        
        let date = formatter.string(from: Date())
        let formattedDate = self.getDateFromString(dateStr: date)
        
        return formattedDate
    }
    
    func getFontName(type : Int) -> String {
        switch type {
        //normals
        case Enums.FONT_TYPE.normal_light:
            return Enums.NormalEnglishFonts.NORMAL_LIGHT
        case Enums.FONT_TYPE.normal_regular:
            return Enums.NormalEnglishFonts.NORMAL_REGULAR
        case Enums.FONT_TYPE.normal_medium:
            return Enums.NormalEnglishFonts.NORMAL_MEDIUM
        case Enums.FONT_TYPE.normal_semibold:
            return Enums.NormalEnglishFonts.NORMAL_SEMIBOLD
        case Enums.FONT_TYPE.normal_bold:
            return Enums.NormalEnglishFonts.NORMAL_BOLD
            
        default:
            return Enums.NormalEnglishFonts.NORMAL_REGULAR
        }
    }
    
    
    func openUrl(str : String) {
        if (str.count > 0 && str.contains(find: "http")) {
            let url = URL(string: str)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    
    func callNumber(phone : String) {
        if (phone.count > 0) {
            if let url = URL(string: "tel://\(phone)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    func startNavigation(longitude : Double, latitude : Double) {
        let sourceSelector = UIAlertController(title: "continueUsing".localized, message: nil, preferredStyle: .actionSheet)
        
        if let popoverController = sourceSelector.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
        }
        
        let googleMapsAction = UIAlertAction(title: "googleMaps".localized, style: .default) { (action) in
            
            
            let url = URL(string: "https://www.google.com/maps/dir/?api=1&destination=\(latitude),\(longitude)&directionsmode=driving")
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
        
        let appleMapsAction = UIAlertAction(title: "appleMaps".localized, style: .default) { (action) in
            
            let coordinate = CLLocationCoordinate2DMake(latitude ,longitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = "\("") \("")"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            
        }
        
        let cancelAction = UIAlertAction(title: "cancel".localized, style: .cancel) { (action) in }
        
        sourceSelector.addAction(googleMapsAction)
        sourceSelector.addAction(appleMapsAction)
        sourceSelector.addAction(cancelAction)
        
        self.present(sourceSelector, animated: true, completion: nil)
    }
    
    func openWhatsApp(phone : String) {
        if (phone.count > 0) {
            if let url = URL(string: "https://api.whatsapp.com/send?phone=\(phone)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else {
            self.showBanner(title: "alert".localized, message: "not_available".localized, type: .error)
        }
    }
    
    
    func getDistanceFloat(fromLatitude : Double, fromLongitude : Double,toLatitude : Double, toLongitude : Double) -> Double {
        let userLatLng = CLLocation(latitude: fromLatitude, longitude: fromLongitude)
        let shopLatLng = CLLocation(latitude: toLatitude, longitude: toLongitude)
        let distanceInMeters = shopLatLng.distance(from: userLatLng)
        let distanceInKM = distanceInMeters / 1000.0
        return distanceInKM
    }
    
    func getDistance(fromLatitude : Double, fromLongitude : Double,toLatitude : Double, toLongitude : Double) -> String {
        let userLatLng = CLLocation(latitude: fromLatitude, longitude: fromLongitude)
        let shopLatLng = CLLocation(latitude: toLatitude, longitude: toLongitude)
        let distanceInMeters = shopLatLng.distance(from: userLatLng)
        let distanceInKM = distanceInMeters / 1000.0
        let distanceStr = String(format: "%.2f", distanceInKM)
        return "\(distanceStr) \("km".localized)"
    }
    
    
    func getDatePicker(withCancel : Bool) -> DatePickerDialog {
        var picker = DatePickerDialog()
        if (withCancel) {
            picker = DatePickerDialog(showCancelButton: true)
        }
        if #available(iOS 13.4, *) {
            picker.datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        return picker
    }
    
    //login handling functions
    func isLoggedIn() -> Bool {
        let userId = getUserId()
        if (userId > 0) {
            return true
        }else {
            self.showAlert(title: "alert".localized, message: "not_logged_in".localized, actionTitle: "login".localized, cancelTitle: "no".localized, actionHandler: {
                //login
                self.presentVC(name: "SignInNav", sb: Storyboards.authentication)
            }) {
                //no
            }
            return false
        }
    }
    
    func isLoggedInNoAction() -> Bool {
        let userId = getUserId()
        if (userId > 0) {
            return true
        }else {
            return false
        }
    }
    
    
    //keychain functions
    func getUserId() -> Int{
        return 0
    }
    
    func getAccessToken() -> String{
        return ""
    }
    
    
    func updateUser(response : String?) {
        let userInfo = try! JSONEncoder().encode(response)
        
        let keychain = KeychainSwift()
        keychain.set(userInfo, forKey: "UserInfo")
    }
    
    func loadUser() -> String?{
        
        let keychain = KeychainSwift()
        let storedData = keychain.getData("UserInfo")
        if storedData != nil {
            let userInfo = try! JSONDecoder().decode(String?.self, from: storedData!)
            return userInfo
        }
        return nil
    }
    
    func deleteUser() {
        let keychain = KeychainSwift()
        keychain.delete("UserInfo")
    }
    
    func isValidPassword(password : String) -> Bool {
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    
    func showTimePicker(completion:@escaping(_ time : Date)-> Void) {
        let minDate = Date().dateAt(.startOfDay)
        let maxDate = Date().dateAt(.endOfDay)
        
        let picker = DatePickerDialog()
        if #available(iOS 13.4, *) {
            picker.datePicker.preferredDatePickerStyle = .wheels
            picker.tintColor = UIColor.colorPrimary
        } else {
            // Fallback on earlier versions
        }
        picker.datePicker.timeZone = .current
        picker.show("select_departure_time".localized, doneButtonTitle: "done".localized, cancelButtonTitle: "cancel".localized, defaultDate: minDate, minimumDate: minDate, maximumDate: maxDate, datePickerMode: .time) { (date) in
            
            completion(date ?? Date())
        }
    }
    
    //biometric
    func saveDataForBiometric(username : String, password : String) {
        let keychain = KeychainSwift()
        keychain.set(username, forKey: "altempo_xmobxilex_biometry")
        keychain.set(password, forKey: "altempo_xpassxwordx_biometry")
    }
    
    
    func getBiometryUsername() -> String {
        let keychain = KeychainSwift()
        return keychain.get("altempo_xmobxilex_biometry") ?? ""
    }
    
    func getBiometryPassword() -> String {
        let keychain = KeychainSwift()
        return keychain.get("altempo_xpassxwordx_biometry") ?? ""
    }
    
    func emptyBiometryData() {
        let keychain = KeychainSwift()
        keychain.delete("altempo_xmobxilex_biometry")
        keychain.delete("altempo_xpassxwordx_biometry")
        keychain.delete("altempo_xtokxenx_biometry")
        UserDefaults.standard.setValue(false, forKey: Enums.USER_DEFAULTS_KEYS.IS_TOUCHID_ACTIVE)
    }
    
    func getLastLatitude() -> Double {
        return UserDefaults.standard.value(forKey: Enums.USER_DEFAULTS_KEYS.LAST_LATITUDE) as? Double ?? 0.0
    }
    func getLastLongitude() -> Double {
        return UserDefaults.standard.value(forKey: Enums.USER_DEFAULTS_KEYS.LAST_LONGITUDE) as? Double ?? 0.0
    }
    
    func saveLastLocation(lat : Double, lng : Double) {
        let defaults = UserDefaults.standard
        defaults.setValue(lat, forKey: Enums.USER_DEFAULTS_KEYS.LAST_LATITUDE)
        defaults.setValue(lng, forKey: Enums.USER_DEFAULTS_KEYS.LAST_LONGITUDE)
    }
    
    func shareAction(content : String) {
        let textToShare = [ content ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    func openAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(Constants.APP_ID)"),
           UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:]) { (opened) in
                    
                }
            } else {
                UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id\(Constants.APP_ID)?mt=8&uo=4")! as URL)
            }
        }
    }
    
    func updateCredentials(accessToken : String? = nil, refreshToken : String? = nil, userId : String? = nil) {
        let tokenJSON = try! JSONEncoder().encode(accessToken)
        let refreshTokenJSON = try! JSONEncoder().encode(refreshToken)
        let userIdJSON = try! JSONEncoder().encode(userId)
        
        let keychain = KeychainSwift()
        keychain.set(tokenJSON, forKey: "accessToken")
        keychain.set(refreshTokenJSON, forKey: "refreshToken")
        keychain.set(userIdJSON, forKey: "userId")
    }
    
    func GetAnnotationUsingCoordinated(_ latitude : Double, _ longitude : Double, completion:@escaping(_ address : String)-> Void) {
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        GMSGeocoder().reverseGeocodeCoordinate(location.coordinate) { (response, error) in
            
            var strAddresMain : String = ""
            if let address : GMSAddress = response?.firstResult() {
                if let lines = address.lines  {
                    if (lines.count > 0) {
                        if lines.count > 0 {
                            if lines[0].count > 0 {
                                strAddresMain = strAddresMain + lines[0]
                            }
                        }
                    }
                    
                    if lines.count > 1 {
                        if lines[1].count > 0 {
                            if strAddresMain.count > 0 {
                                strAddresMain = strAddresMain + ", \(lines[1])"
                            } else {
                                strAddresMain = strAddresMain + "\(lines[1])"
                            }
                        }
                    }
                    
                    if (strAddresMain.count > 0) {
                        
                        var strSubTitle = ""
                        if let locality = address.locality {
                            strSubTitle = locality
                        }
                        
                        if let administrativeArea = address.administrativeArea {
                            if strSubTitle.count > 0 {
                                strSubTitle = "\(strSubTitle), \(administrativeArea)"
                            }
                            else {
                                strSubTitle = administrativeArea
                            }
                        }
                        
                        if let country = address.country {
                            if strSubTitle.count > 0 {
                                strSubTitle = "\(strSubTitle), \(country)"
                            }
                            else {
                                strSubTitle = country
                            }
                        }
                        
                        completion(strAddresMain)
                        
                    }
                    else {
                        completion("Loading".localized)
                    }
                }
                else {
                    completion("Loading".localized)
                }
            }
            else {
                completion("Loading".localized)
            }
        }
    }
    
    func updateUserLocation(_ lat : Double?, _ lng : Double?) {
        let defautls = UserDefaults.standard
        defautls.setValue(lat ?? 0.0, forKey: Enums.USER_DEFAULTS_KEYS.LAST_LATITUDE)
        defautls.setValue(lng ?? 0.0, forKey: Enums.USER_DEFAULTS_KEYS.LAST_LONGITUDE)
    }
    
    func isLocationPermissionGranted(completion:@escaping(_ isGranted : Bool)-> Void) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                completion(false)
            case .authorizedAlways, .authorizedWhenInUse:
                completion(true)
            @unknown default:
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
}
