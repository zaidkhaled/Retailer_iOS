//
//  CompletedOrderViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

enum CompletedOrderSection {
    case customer
    case itemsSpacing
    case items
    case totalSpacing
    case total
}

class CompletedOrderViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var completedOrdersTableView: UITableView!
    
    private var sections: [CompletedOrderSection] = [.customer, .itemsSpacing, .items, .totalSpacing, .total]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewUI()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        completedOrdersTableView.reloadData()
    }
    
    private func registerTableViewUI() {
        //cells
        completedOrdersTableView.register(UINib.init(nibName: "ReviewTotalItemsCell", bundle: nil), forCellReuseIdentifier: "ReviewTotalItemsCell")
        completedOrdersTableView.register(UINib.init(nibName: "ReviewProductCell", bundle: nil), forCellReuseIdentifier: "ReviewProductCell")
        completedOrdersTableView.register(UINib.init(nibName: "CustomerInfoCell", bundle: nil), forCellReuseIdentifier: "CustomerInfoCell")
        completedOrdersTableView.register(UINib.init(nibName: "EmptyCell", bundle: nil), forCellReuseIdentifier: "EmptyCell")
    }
    
    private func setupTableView() {
        completedOrdersTableView.delegate = self
        completedOrdersTableView.dataSource = self
        completedOrdersTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    
    //tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionType = sections[section]
        switch sectionType {
        case .customer, .itemsSpacing, .totalSpacing, .total:
            return 1
        case .items:
            return 6
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType = sections[indexPath.section]
        
        switch sectionType {
        case .customer:
            let cell: CustomerInfoCell  = tableView.dequeueReusableCell(withIdentifier: "CustomerInfoCell") as! CustomerInfoCell
            
            return cell
            
        case .itemsSpacing:
            let cell: EmptyCell  = tableView.dequeueReusableCell(withIdentifier: "EmptyCell") as! EmptyCell
            
            return cell
            
        case .items:
            let cell: ReviewProductCell  = tableView.dequeueReusableCell(withIdentifier: "ReviewProductCell") as! ReviewProductCell
            
            return cell
            
        case .totalSpacing:
            let cell: EmptyCell  = tableView.dequeueReusableCell(withIdentifier: "EmptyCell") as! EmptyCell
            
            return cell
            
        case .total:
            let cell: ReviewTotalItemsCell  = tableView.dequeueReusableCell(withIdentifier: "ReviewTotalItemsCell") as! ReviewTotalItemsCell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionType = sections[indexPath.section]
        
        switch sectionType {
        case .customer:
            return 85.0
        case .itemsSpacing:
            return 60.0
        case .items:
            return 60.0
        case .totalSpacing:
            return 50.0
        case .total:
            return 70.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
