//
//  ReviewProductCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class ReviewProductCell: BaseTVCell {
    //60

    @IBOutlet weak var lblCount: NormalLabel!
    
    @IBOutlet weak var ivProduct: UIImageView!
    
    @IBOutlet weak var lblName: NormalLabel!

}
