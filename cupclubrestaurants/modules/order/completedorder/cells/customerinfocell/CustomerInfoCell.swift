//
//  CustomerInfoCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class CustomerInfoCell: BaseTVCell {
    //85
    @IBOutlet weak var lblCustomerId: NormalLabel!
    
    @IBOutlet weak var lblDate: NormalLabel!
    
    @IBOutlet weak var lblTime: NormalLabel!
}
