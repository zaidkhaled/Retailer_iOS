//
//  ScanItemsViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 19/05/2021.
//

import UIKit
import MercariQRScanner

class ScanItemsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewFlashLight: CardView!
    @IBOutlet weak var btnFlashLight: UIButton!
    private var isTorchActive : Bool = false
    
    @IBOutlet weak var viewCameraContainer: UIView!
    
    private var scanner: QRScannerView?
    
    //add remove button
    @IBOutlet weak var viewAddRemove: UIView!
    @IBOutlet weak var btnAddRemove: NormalButton!
    @IBOutlet weak var ivAddRemove: UIImageView!
    
    //add remove labels
    @IBOutlet weak var lblScreenTitle: NormalLabel!
    @IBOutlet weak var lblScanItems: NormalLabel!
    
    private var isAddingItems : Bool = true
    
    //items table
    @IBOutlet weak var scannedItemsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewUI()
        setupTableView()
    }
    
    private func registerTableViewUI() {
        //cells
        scannedItemsTableView.register(UINib.init(nibName: "ScannedItemCell", bundle: nil), forCellReuseIdentifier: "ScannedItemCell")
    }
    
    private func setupTableView() {
        scannedItemsTableView.delegate = self
        scannedItemsTableView.dataSource = self
        scannedItemsTableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    private func initQRCodeScanner() {
        if (scanner == nil) {
            scanner = QRScannerView(frame: viewCameraContainer.bounds)
            if let scannerView = scanner {
                scannerView.focusImage = nil
                viewCameraContainer.addSubview(scannerView)
                scannerView.configure(delegate: self)
                scannerView.startRunning()
            }
        }
    }
    
    private func setAddingItemsScenario() {
        lblScreenTitle.text = "add_items".localized
        lblScanItems.text = "scan_qr_to_add_items".localized
        btnAddRemove.setTitle("remove_item".localized.uppercased(), for: .normal)
        ivAddRemove.image = UIImage(named: "ic_minus")
    }
    
    private func setRemovingItemsScenario() {
        lblScreenTitle.text = "remove_items".localized
        lblScanItems.text = "scan_qr_to_remove_items".localized
        btnAddRemove.setTitle("add_item".localized.uppercased(), for: .normal)
        ivAddRemove.image = UIImage(named: "ic_plus")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initQRCodeScanner()
        scannedItemsTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopRunning()
        super.viewWillDisappear(animated)
    }
    
    
    //tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ScannedItemCell  = tableView.dequeueReusableCell(withIdentifier: "ScannedItemCell") as! ScannedItemCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func flashLightPressed(_ sender: Any) {
        if (isTorchActive) {
            isTorchActive = false
            self.scanner?.setTorchActive(isOn: false)
            self.viewFlashLight.backgroundColor = UIColor.black
        }else {
            isTorchActive = true
            self.scanner?.setTorchActive(isOn: true)
            self.viewFlashLight.backgroundColor = UIColor.colorAccent
        }
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        pushVC(name: "OrderSummaryViewController", sb: Storyboards.order)
    }
    
    @IBAction func addRemovePressed(_ sender: Any) {
        if (isAddingItems) {
            isAddingItems = false
            setRemovingItemsScenario()
        }else {
            isAddingItems = true
            setAddingItemsScenario()
        }
    }
    
    
}

extension ScanItemsViewController : QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
       // self.showBanner(title: "alert".localized, message: "camera_permission_needed".localized, type: Enums.APP_MESSAGE.info)
       }

       func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
           print(code)
       }
}
