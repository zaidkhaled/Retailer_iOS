//
//  CancelOrderPopup.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 19/05/2021.
//

import UIKit

protocol CancelOrderPopupProtocol {
    func onCancelOrder()
}
class CancelOrderPopup: BaseViewController {

    var delegate : CancelOrderPopupProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func overlayPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func noPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func yesPressed(_ sender: Any) {
        delegate?.onCancelOrder()
    }

}
