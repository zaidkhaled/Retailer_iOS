//
//  OrderSubmittedViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 19/05/2021.
//

import UIKit

class OrderSubmittedViewController: BaseViewController {
    
    @IBOutlet weak var lblThanks: NormalLabel!
    
    @IBOutlet weak var lblOrderSubmitted: NormalLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startClosingTimer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateViews()
    }
    
    private func animateViews() {
        self.lblThanks.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        self.lblOrderSubmitted.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.5, animations: {() -> Void in
            self.lblThanks.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.lblOrderSubmitted.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    private func startClosingTimer() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.goHome()
        }
    }
    
}
