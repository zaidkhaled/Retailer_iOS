//
//  ReturnBagPopup.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 19/05/2021.
//

import UIKit

protocol ReturnBagPopupProtocol {
    func addReturnBag(add : Bool)
}
class ReturnBagPopup: BaseViewController {

    var delegate : ReturnBagPopupProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func overlayPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func noPressed(_ sender: Any) {
        delegate?.addReturnBag(add: false)
    }
    
    @IBAction func yesPressed(_ sender: Any) {
        delegate?.addReturnBag(add: true)
    }
    
}
