//
//  ScanCustomerViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit
import CBPinEntryView
import MercariQRScanner

class ScanCustomerViewController: BaseViewController, CBPinEntryViewDelegate {
    
    @IBOutlet weak var viewFlashLight: CardView!
    @IBOutlet weak var btnFlashLight: UIButton!
    private var isTorchActive : Bool = false
    
    @IBOutlet weak var viewCameraContainer: UIView!
    
    @IBOutlet weak var customerIdPinView: CBPinEntryView!
    
    private var scanner: QRScannerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpPinView()
    }
    
    private func setUpPinView() {
        customerIdPinView.delegate = self
    }
    
    func entryChanged(_ completed: Bool) {
        //nth
    }
    
    func entryCompleted(with entry: String?) {
        customerIdPinView.resignFirstResponder()
    }
    
    private func initQRCodeScanner() {
        if (scanner == nil) {
            scanner = QRScannerView(frame: viewCameraContainer.bounds)
            if let scannerView = scanner {
                scannerView.focusImage = nil
                viewCameraContainer.addSubview(scannerView)
                scannerView.configure(delegate: self)
                scannerView.startRunning()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initQRCodeScanner()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopRunning()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func flashLightPressed(_ sender: Any) {
        if (isTorchActive) {
            isTorchActive = false
            self.scanner?.setTorchActive(isOn: false)
            self.viewFlashLight.backgroundColor = UIColor.black
        }else {
            isTorchActive = true
            self.scanner?.setTorchActive(isOn: true)
            self.viewFlashLight.backgroundColor = UIColor.colorAccent
        }
    }
    
    @IBAction func continuePressed(_ sender: Any) {
        let stringPin = customerIdPinView.getPinAsString().trim().replacedArabicDigitsWithEnglish
        if (stringPin.count == Enums.VALIDATIONS.pin) {
            pushVC(name: "ScanItemsViewController", sb: Storyboards.order)
        }else {
            self.showBanner(title: "alert".localized, message: "enter_customer_id".localized, type: Enums.APP_MESSAGE.info)
        }
    }
}


extension ScanCustomerViewController : QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
       // self.showBanner(title: "alert".localized, message: "camera_permission_needed".localized, type: Enums.APP_MESSAGE.info)
       }

       func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
           print(code)
       }
}
