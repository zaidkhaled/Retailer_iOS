//
//  HistoryOrderCell.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class HistoryOrderCell: BaseTVCell {
    //70
    
    @IBOutlet weak var viewHighlight: UIView!

    @IBOutlet weak var lblDate: NormalLabel!
    
    @IBOutlet weak var lblTime: NormalLabel!
    
    @IBOutlet weak var lblCustomerId: NormalLabel!
    
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if (highlighted){
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                self.viewHighlight.backgroundColor = UIColor.highlighted
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                self.viewHighlight.backgroundColor = UIColor.white
            }, completion: nil)
        }
    }
    
}
