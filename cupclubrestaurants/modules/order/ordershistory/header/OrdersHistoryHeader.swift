//
//  OrdersHistoryHeader.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

class OrdersHistoryHeader: BaseHeaderFooterView {
//55
    @IBOutlet weak var lblTitle: NormalLabel!
    
    @IBOutlet weak var lblCount: NormalLabel!
    
}
