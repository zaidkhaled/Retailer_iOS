//
//  OrdersHistoryViewController.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 18/05/2021.
//

import UIKit

enum OrdersHistorySection {
    case draft
    case completed
}
class OrdersHistoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var ordersTableView: UITableView!
    
    private var sections: [OrdersHistorySection] = [.draft, .completed]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewUI()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ordersTableView.reloadData()
    }
    
    private func registerTableViewUI() {
        //headers
        ordersTableView.register(UINib.init(nibName: "OrdersHistoryHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "OrdersHistoryHeader")
        //cells
        ordersTableView.register(UINib.init(nibName: "HistoryOrderCell", bundle: nil), forCellReuseIdentifier: "HistoryOrderCell")
    }
    
    private func setupTableView() {
        ordersTableView.delegate = self
        ordersTableView.dataSource = self
        ordersTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    
    //tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionType = sections[section]
        switch sectionType {
        case .draft:
            return 4
        case .completed:
            return 8
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType = sections[indexPath.section]
        
        switch sectionType {
        case .draft:
            let cell: HistoryOrderCell  = tableView.dequeueReusableCell(withIdentifier: "HistoryOrderCell") as! HistoryOrderCell
            
            return cell
            
        case .completed:
            let cell: HistoryOrderCell  = tableView.dequeueReusableCell(withIdentifier: "HistoryOrderCell") as! HistoryOrderCell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //-----
        let ordersHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "OrdersHistoryHeader") as? OrdersHistoryHeader
        
        let sectionType = sections[section]
        switch sectionType {
        case .draft:
            ordersHeader?.lblTitle.text = "draft_orders".localized
            ordersHeader?.lblCount.text = "4"
            return ordersHeader
        case .completed:
            ordersHeader?.lblTitle.text = "completed_orders".localized
            ordersHeader?.lblCount.text = "8"
            return ordersHeader
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionType = sections[indexPath.section]
        if (sectionType == .draft) {
            pushVC(name: "CompletedOrderViewController", sb: Storyboards.order)
        }else {
            pushVC(name: "OrderSummaryViewController", sb: Storyboards.order)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = UIColor.windowColor
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
