//
//  MyProfileResponse.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 16/05/2021.
//

import Foundation

// MARK: - MyProfileResponse
struct MyProfileResponse: Codable {
    let type, id: String?
    let attributes: Attributes?
}

// MARK: - Attributes
struct Attributes: Codable {
    let email, name, currency, status: String?
    let verified: Bool?
    let paymentStatus, paymentTokenName, braintreeCustomerID, squareCustomerID: String?
    let squareCardID, deviceID, image: String?
    let points, pointsMax: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case email, name, currency, status, verified
        case paymentStatus = "payment_status"
        case paymentTokenName = "payment_token_name"
        case braintreeCustomerID = "braintree_customer_id"
        case squareCustomerID = "square_customer_id"
        case squareCardID = "square_card_id"
        case deviceID = "device_id"
        case image, points
        case pointsMax = "points_max"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
