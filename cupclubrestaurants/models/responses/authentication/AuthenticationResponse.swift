//
//  AuthenticationResponse.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 16/05/2021.
//

import Foundation

// MARK: - AuthenticationResponse
struct AuthenticationResponse: Codable {
    let userID, tokenType: String?
    let expiresIn: Int?
    let accessToken, refreshToken: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
    }
}
