//
//  BottomCardView.swift
//  FlyUsVip
//
//  Created by Zaid Khaled on 1/13/21.
//

import UIKit

@IBDesignable
class BottomCardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 8
    
    override func layoutSubviews() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
}
