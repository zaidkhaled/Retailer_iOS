//
//  BorderImage.swift
//  altempuser
//
//  Created by Zaid Khaled on 07/03/2021.
//

import Foundation
import UIKit

@IBDesignable
class BorderImage : UIImageView {
    override func layoutSubviews() {
        self.layer.cornerRadius = self.bounds.width / 2.0
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.buttonColor.cgColor
    }
}
