//
//  TopView.swift
//  FlyUsVip
//
//  Created by Zaid Khaled on 1/14/21.
//

import UIKit

@IBDesignable
class TopView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 8
    
    override func layoutSubviews() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
}
