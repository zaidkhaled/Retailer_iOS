//
//  TopCorneredImage.swift
//  FlyUsVip
//
//  Created by Zaid Khaled on 1/12/21.
//

import UIKit

@IBDesignable
class TopCorneredImage: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 8
    
    override func layoutSubviews() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
}
