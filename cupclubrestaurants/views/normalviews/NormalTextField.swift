//
//  MyUITextField.swift
//  rzq
//
//  Created by Zaid najjar on 3/31/19.
//  Copyright © 2019 technzone. All rights reserved.
//

import Foundation
import UIKit

//@IBDesignable
class NormalTextField : UITextField {
    @IBInspectable var font_type: String = "15,1" {
        didSet {
            let result = font_type.split(separator: ",")
            let strSize = String(result[0])
            let fontSize: CGFloat = CGFloat((strSize as NSString).doubleValue)
            let fontType = String(result[1])
            switch fontType {
            case "1"://light
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_LIGHT)", size: (fontSize))
                break
            case "2"://regular
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_REGULAR)", size: (fontSize))
                break
            case "3"://medium
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_MEDIUM)", size: (fontSize))
                break
            case "4"://semibold
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_SEMIBOLD)", size: (fontSize))
                break
            case "5"://bold
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_BOLD)", size: (fontSize))
                break
            default:
                self.font = UIFont(name: "\(Enums.NormalEnglishFonts.NORMAL_REGULAR)", size: (fontSize))
                break
            }
            
        }
    }
    
    @IBInspectable var paddingLeftCustom: CGFloat {
            get {
                return leftView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                leftView = paddingView
                leftViewMode = .always
            }
        }

        @IBInspectable var paddingRightCustom: CGFloat {
            get {
                return rightView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                rightView = paddingView
                rightViewMode = .always
            }
        }
}
