//
//  Constants.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import Foundation

class Constants {
    
    static let APP_ID = ""
    
    static let GOOGLE_API_KEY = ""
    
    //NOTIFICATIONS
    static let NOTIFICATION_COUNT = "NOTIFICATION_COUNT"
    static let NOT_KEY_TITLE_EN = "TitleEn"
    static let NOT_KEY_BODY_EN = "MessageEn"
    static let NOT_KEY_TYPE = "Type"
    static let NOT_KEY_VALUE = "NotificationData"
    
    //OAUTH
    enum NETWORK {
        static let CLIENT_ID = "6"
        static let CLIENT_SECRET = "RbrVf0gXU26brsHIe4aNHRI38vYueodVLRW3XkQv"
        static let WEB_URL = "https://api-stage.cupclub.com/oauth/authorize"
        static let FALLBACK_URL = "cupclubretailer://api-stage.cupclub.com"
        static let Bearer = "Bearer"
        static let Authorization = "Authorization"
    }
    
   
}
