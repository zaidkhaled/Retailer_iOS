//
//  Enums.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import Foundation
import Hero

class Enums {
    
    //userdefaults keys
    enum USER_DEFAULTS_KEYS {
        static let DID_SELECT_LANGUAGE = "DID_SELECT_LANGUAGE"
        static let DID_SEE_INTRO = "DID_SEE_INTRO"
        static let IS_TOUCHID_ACTIVE = "IS_TOUCHID_ACTIVE"
        static let IS_NOTIFICATIONS_ON = "IS_NOTIFICATIONS_ON"
        static let LAST_LATITUDE = "LAST_LATITUDE"
        static let LAST_LONGITUDE = "LAST_LONGITUDE"
    }
    
    //app messages
    enum APP_MESSAGE {
        case error
        case success
        case info
        case warning
    }
    
    //validations
    enum VALIDATIONS {
        static let pin = 6
    }
    
    //normal fonts
    public enum NormalEnglishFonts {
        static let NORMAL_LIGHT = "Montserrat-Light"
        static let NORMAL_REGULAR = "Montserrat-Regular"
        static let NORMAL_MEDIUM = "Montserrat-Medium"
        static let NORMAL_SEMIBOLD = "Montserrat-Bold"
        static let NORMAL_BOLD = "Montserrat-Black"
    }
    
    //date formats
    enum DATE_FORMATS {
        static let dd_mm_yyyy = "dd/MM/yyyy"
        static let hh_mm_a = "hh:mm a"
        static let hh_mm = "HH:mm"
        static let dd_MMM = "dd-MMM"
        static let mmm_yyyy = "MMM yyyy"
        static let MMM_dd_yyyy = "MMM dd,yyyy"
        static let MMM_dd_yyyy_hh_mm = "MMM dd,yyyy, HH:mm"
        static let EEEE_MMM_d_yyyy = "EEEE, MMM d, yyyy"
        static let api_date = "yyyy-MM-dd'T'HH:mm:ss"
        static let returned_date = "yyyy-MM-dd'T'HH:mm:ss"
        static let EEEE = "EEEE"
        static let MMM = "MMM"
        static let dd = "dd"
        static let MMM_dd_HH_mm = "MMM/dd HH:mm"
    }
    
    //font types
    enum FONT_TYPE {
        static let normal_light = 1
        static let normal_regular = 2
        static let normal_medium = 3
        static let normal_semibold = 4
        static let normal_bold = 5
    }
    
    //animations
    enum ANIMATIONS {
        static let present_animation : HeroDefaultAnimationType = .zoomOut
    }
}
