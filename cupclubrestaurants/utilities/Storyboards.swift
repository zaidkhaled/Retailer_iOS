//
//  Storyboards.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import Foundation
import UIKit

struct Storyboards {
    static var authentication: UIStoryboard {
        return UIStoryboard(name: "Authentication", bundle: nil)
    }
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    static var core: UIStoryboard {
        return UIStoryboard(name: "Core", bundle: nil)
    }
    static var order: UIStoryboard {
        return UIStoryboard(name: "Order", bundle: nil)
    }
    static var more: UIStoryboard {
        return UIStoryboard(name: "More", bundle: nil)
    }
}
