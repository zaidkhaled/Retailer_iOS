//
//  Repository.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 16/05/2021.
//

import Moya

protocol Networkable {
    var provider: MoyaProvider<API> { get }

    func authenticate(username : String, password : String, completion: @escaping (Result<AuthenticationResponse, Error>) -> ())
    
    func getProfile(completion: @escaping (Result<MyProfileResponse, Error>) -> ())
}

class NetworkManager: Networkable {
    
    func getProfile(completion: @escaping (Result<MyProfileResponse, Error>) -> ()) {
        request(target: .getProfile, completion: completion)
    }
    
    var provider = MoyaProvider<API>(plugins: [NetworkLoggerPlugin()])
    
    func authenticate(username: String, password: String, completion: @escaping (Result<AuthenticationResponse, Error>) -> ()) {
        request(target: .authenticate(username: username, password: password), completion: completion)
    }
}

private extension NetworkManager {
    private func request<T: Decodable>(target: API, completion: @escaping (Result<T, Error>) -> ()) {
        provider.request(target) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(T.self, from: response.data)
                    switch response.statusCode {
                    case 200:
                        completion(.success(results))
                        break
                    case 401:
                        completion(.failure(NSError(domain: "not authorized", code: response.statusCode, userInfo:nil)))
                        break
                    default:
                        completion(.success(results))
                    }
                } catch let error {
                    completion(.failure(error))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
