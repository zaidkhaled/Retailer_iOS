//
//  API.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 16/05/2021.
//

import Moya

enum API {
    case authenticate(username : String, password : String)
    case getProfile
}

extension API: TargetType {
    
    var headers: [String : String]? {
        return [Constants.NETWORK.Authorization : "\(Constants.NETWORK.Bearer) \(accessToken)"]
    }
    
    var baseURL: URL {
        guard let url = URL(string: "https://api-stage.cupclub.com/") else { fatalError() }
        return url
    }
    
    var path: String {
        switch self {
        case .authenticate:
            return "oauth/token"
        case .getProfile:
            return "users/me"
        }
    }
    
    var method: Method {
        switch self {
        case .authenticate:
            return .post
        case .getProfile:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .authenticate(let username, let password):
            return .requestParameters(parameters: ["username": username, "password" : password, "grant_type" : "password", "client_id" : "3", "client_secret" : "4fpS8RsmsQXJPvCGptv9LIgOrlewmVffY8sKiHWk", "scope" : ""], encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    private var accessToken : String {
        let keychain = KeychainSwift()
        let storedAccessToken = keychain.getData("accessToken")
        if storedAccessToken != nil {
            let accessToken = try! JSONDecoder().decode(String.self, from: storedAccessToken!)
            return accessToken
        }
        return ""
    }
}
