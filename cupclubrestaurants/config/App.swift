//
//  App.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import Foundation
import CoreFoundation

class App: NSObject {
    
    //clicked notification
    var notificationType: String?
    var notificationValue: String?
    
    // MARK:- Singleton
    static var shared : App = {
        let instance = App()
        return instance
    }()
    
}
