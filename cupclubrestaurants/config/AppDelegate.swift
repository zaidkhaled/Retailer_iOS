//
//  AppDelegate.swift
//  cupclubrestaurants
//
//  Created by Zaid Khaled on 08/05/2021.
//

import Alamofire
import IQKeyboardManagerSwift
import netfox
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseMessaging
import SwiftEventBus
import AJMessage
import Toast_Swift

var AFManager = Session()

@main
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate ,UNUserNotificationCenterDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //google maps / places keys
        GMSServices.provideAPIKey("\(Constants.GOOGLE_API_KEY)")
        GMSPlacesClient.provideAPIKey("\(Constants.GOOGLE_API_KEY)")
        
        //keyboard global settings
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        //Alamofire shared instance
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20 // seconds
        configuration.timeoutIntervalForResource = 20 //seconds
        AFManager = Alamofire.Session(configuration: configuration)
        
        //network interceptor
         #if DEBUG
         NFX.sharedInstance().start()
         #endif
        
        // firebase & remote notifications
//        FirebaseApp.configure()
//        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        //authrorization event bus
        SwiftEventBus.onMainThread(self, name: "shouldReLogin") { result in
           //open login screen
           // self.signIn()
        }
        
        //toast config
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        
        return true
    }
    
    
    //notifications protocol functions
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        //update device token here
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        
        if(application.applicationState == .active) {
            print("user tapped the notification bar when the app is in foreground")
            let type = response.notification.request.content.userInfo[Constants.NOT_KEY_TYPE] as? String ?? "0"
            let itemId = response.notification.request.content.userInfo[Constants.NOT_KEY_VALUE] as? String ?? "0"
            App.shared.notificationType = type
            App.shared.notificationValue = itemId
            
            
        }
        
        if(application.applicationState == .inactive)
        {
            print("user tapped the notification bar when the app is in background")
            let type = response.notification.request.content.userInfo[Constants.NOT_KEY_TYPE] as? String ?? "0"
            let itemId = response.notification.request.content.userInfo[Constants.NOT_KEY_VALUE] as? String ?? "0"
            
            App.shared.notificationType = type
            App.shared.notificationValue = itemId
            
            
        }
        
        /* Change root view controller to a specific viewcontroller */
        // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // let vc = storyboard.instantiateViewController(withIdentifier: "ViewControllerStoryboardID") as? ViewController
        // self.window?.rootViewController = vc
        
        completionHandler()
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        //  LabasNotificationsManager.shared.addNotification(userInfo: userInfo)
        
        
        var title = ""
        var body = ""
        var type = ""
        
        title = userInfo[Constants.NOT_KEY_TITLE_EN] as? String ?? ""
        body = userInfo[Constants.NOT_KEY_BODY_EN] as? String ?? ""
        
        type = userInfo[Constants.NOT_KEY_TYPE] as? String ?? "0"
        
        
        App.shared.notificationType = type
        App.shared.notificationValue = "0"
        
        scheduleNotifications(title: title , message: body, type : type, itemId: "0")
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        var title = ""
        var body = ""
        var type = ""
        
        title = userInfo[Constants.NOT_KEY_TITLE_EN] as? String ?? ""
        body = userInfo[Constants.NOT_KEY_BODY_EN] as? String ?? ""
        
        type = userInfo[Constants.NOT_KEY_TYPE] as? String ?? "0"
        
        App.shared.notificationType = type
        App.shared.notificationValue = "0"
        
        let state: UIApplication.State = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
        
        if state == .background {
            scheduleNotifications(title: title , message: body,type: type,itemId: "0")
        } else if state == .active {
            if (title.count > 0 || body.count > 0) {
                self.showBanner(title: title, message: body, type: .info)
            }
        }
        
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func showBanner(title:String, message:String, type : Enums.APP_MESSAGE) {
        switch type {
        case .info:
            AJMessage.show(title: title, message: message, duration: 3.0, position: .top, status: .info)
            break
        case .success:
            AJMessage.show(title: title, message: message, duration: 3.0, position: .top, status: .success)
            break
        case .error:
            AJMessage.show(title: title, message: message, duration: 3.0, position: .top, status: .error)
            break
        default:
            AJMessage.show(title: title, message: message, duration: 3.0, position: .top, status: .info)
        }
    }
    
    //branch
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        return true
    }
    //branch
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
       
        return true
    }
    
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        //do any URL scheme config
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    
    func updateNotificationCount() {
        let defaults = UserDefaults.standard
        var notificationCount : Int = defaults.value(forKey: Constants.NOTIFICATION_COUNT) as? Int ?? 0
        notificationCount = notificationCount + 1
        defaults.setValue(notificationCount, forKey: Constants.NOTIFICATION_COUNT)
    }
    
    
    func scheduleNotifications(title : String, message : String, type : String, itemId : String) {
        
        self.updateNotificationCount()
        
        let requestIdentifier = "Notification"
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            
            content.badge = 1
            content.title = title
            content.subtitle = "appname".localized
            content.body = message
            content.categoryIdentifier = "actionCategory"
            content.sound = UNNotificationSound.default
            //custom tune
          //  content.sound =  UNNotificationSound(named: UNNotificationSoundName(rawValue: "app_tone.caf"))
            
            
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
            let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { (error:Error?) in
                
                if error != nil {
                    print(error?.localizedDescription ?? "not localized")
                }
                print("Notification Register Success")
            }
            
        } else {
            // Fallback on earlier versions
            let content = UILocalNotification()
            
            content.alertTitle = title
            content.alertBody = message
            content.category = ""
            
        }
        
    }


}

